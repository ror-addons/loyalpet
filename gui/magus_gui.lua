--Labels
LPET.OPTIONS.Windows.LPETOptionsDaemonicFireLabel= {type="label", career=LPET_CONSTANTS.MAGUS, label=LPET.LOCALS.OPTION.DAEMONICFIRETITLE.label, tooltiptext=LPET.LOCALS.OPTION.DAEMONICFIRETITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsWarpingEnergyLabel= {type="label", career=LPET_CONSTANTS.MAGUS, label=LPET.LOCALS.OPTION.WARPINGENERGYTITLE.label, tooltiptext=LPET.LOCALS.OPTION.WARPINGENERGYTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsFlameOfTzeentchLabel= {type="label", career=LPET_CONSTANTS.MAGUS, label=LPET.LOCALS.OPTION.FLAMEOFTZEENTCHTITLE.label, tooltiptext=LPET.LOCALS.OPTION.FLAMEOFTZEENTCHTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsFlamesOfChangeLabel= {type="label", career=LPET_CONSTANTS.MAGUS, label=LPET.LOCALS.OPTION.FLAMESOFCHANGETITLE.label, tooltiptext=LPET.LOCALS.OPTION.FLAMESOFCHANGETITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsCoruscatingEnergyLabel= {type="label", career=LPET_CONSTANTS.MAGUS, label=LPET.LOCALS.OPTION.CORUSCATINGENERGYTITLE.label, tooltiptext=LPET.LOCALS.OPTION.CORUSCATINGENERGYTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsDaemonicConsumptionLabel= {type="label", career=LPET_CONSTANTS.MAGUS, label=LPET.LOCALS.OPTION.DAEMONICCONSUMPTIONTITLE.label, tooltiptext=LPET.LOCALS.OPTION.DAEMONICCONSUMPTIONTITLE.tooltiptext}

--Combos
LPET.OPTIONS.Windows.LPETOptionsDemonCombo = {type="combo", career=LPET_CONSTANTS.MAGUS, label = L"", tooltiptext = L"Use this to set all modes to same value at once", value = LPET.quickMode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsDaemonicFireCombo = {type="combo", career=LPET_CONSTANTS.MAGUS, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.DaemonicFire.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsWarpingEnergyCombo = {type="combo", career=LPET_CONSTANTS.MAGUS, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.WarpingEnergy.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsFlameOfTzeentchCombo = {type="combo", career=LPET_CONSTANTS.MAGUS, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.FlameOfTzeentch.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsFlamesOfChangeCombo = {type="combo", career=LPET_CONSTANTS.MAGUS, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.FlamesOfChange.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsCoruscatingEnergyCombo = {type="combo", career=LPET_CONSTANTS.MAGUS, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.CoruscatingEnergy.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsDaemonicConsumptionCombo = {type="combo", career=LPET_CONSTANTS.MAGUS, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.DaemonicConsumption.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}

LPET.OPTIONS.Windows.LPETOptionsDemonPriorityCombo = {type="combo", career=LPET_CONSTANTS.MAGUS, label = L"", tooltiptext = L"Use this to set all priorities to same value at once", value = LPET.quickPriority, data = LPET.LOCALS.OPTION.PRIORITY2.data}
LPET.OPTIONS.Windows.LPETOptionsDaemonicFirePriorityCombo = {type="combo", career=LPET_CONSTANTS.MAGUS, label = LPET.LOCALS.OPTION.PRIORITY2.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY2.tooltiptext, value = LPET_CONFIG.DaemonicFire.priority, data = LPET.LOCALS.OPTION.PRIORITY2.data}
LPET.OPTIONS.Windows.LPETOptionsWarpingEnergyPriorityCombo = {type="combo", career=LPET_CONSTANTS.MAGUS, label = LPET.LOCALS.OPTION.PRIORITY2.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY2.tooltiptext, value = LPET_CONFIG.WarpingEnergy.priority, data = LPET.LOCALS.OPTION.PRIORITY2.data}
LPET.OPTIONS.Windows.LPETOptionsFlameOfTzeentchPriorityCombo = {type="combo", career=LPET_CONSTANTS.MAGUS, label = LPET.LOCALS.OPTION.PRIORITY2.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY2.tooltiptext, value = LPET_CONFIG.FlameOfTzeentch.priority, data = LPET.LOCALS.OPTION.PRIORITY2.data}
LPET.OPTIONS.Windows.LPETOptionsFlamesOfChangePriorityCombo = {type="combo", career=LPET_CONSTANTS.MAGUS, label = LPET.LOCALS.OPTION.PRIORITY2.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY2.tooltiptext, value = LPET_CONFIG.FlamesOfChange.priority, data = LPET.LOCALS.OPTION.PRIORITY2.data}
LPET.OPTIONS.Windows.LPETOptionsCoruscatingEnergyPriorityCombo = {type="combo", career=LPET_CONSTANTS.MAGUS, label = LPET.LOCALS.OPTION.PRIORITY2.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY2.tooltiptext, value = LPET_CONFIG.CoruscatingEnergy.priority, data = LPET.LOCALS.OPTION.PRIORITY2.data}
LPET.OPTIONS.Windows.LPETOptionsDaemonicConsumptionPriorityCombo = {type="combo", career=LPET_CONSTANTS.MAGUS, label = LPET.LOCALS.OPTION.PRIORITY2.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY2.tooltiptext, value = LPET_CONFIG.DaemonicConsumption.priority, data = LPET.LOCALS.OPTION.PRIORITY2.data}

LPET.OPTIONS.Windows.LPETOptionsDemonHealthCombo = {type="combo", career=LPET_CONSTANTS.MAGUS, label = L"", tooltiptext = L"Use this to set all health limits to same value at once", value = (LPET.quickHealthLimit / 5) + 1, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsDaemonicFireHealthCombo = {type="combo", career=LPET_CONSTANTS.MAGUS, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = (LPET_CONFIG.DaemonicFire.healthLimit / 5) + 1, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsWarpingEnergyHealthCombo = {type="combo", career=LPET_CONSTANTS.MAGUS, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = (LPET_CONFIG.WarpingEnergy.healthLimit / 5) + 1, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsFlameOfTzeentchHealthCombo = {type="combo", career=LPET_CONSTANTS.MAGUS, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = (LPET_CONFIG.FlameOfTzeentch.healthLimit / 5) + 1, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsFlamesOfChangeHealthCombo = {type="combo", career=LPET_CONSTANTS.MAGUS, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = (LPET_CONFIG.FlamesOfChange.healthLimit / 5) + 1, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsCoruscatingEnergyHealthCombo = {type="combo", career=LPET_CONSTANTS.MAGUS, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = (LPET_CONFIG.CoruscatingEnergy.healthLimit / 5) + 1, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsDaemonicConsumptionHealthCombo = {type="combo", career=LPET_CONSTANTS.MAGUS, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = (LPET_CONFIG.DaemonicConsumption.healthLimit / 5) + 1, data = LPET.LOCALS.OPTION.HEALTH.data}

-- Functions
function LPET.UpdateMagusGUI()
  LPET.OPTIONS.Windows.LPETOptionsDemonCombo.value = LPET.quickMode
  LPET.OPTIONS.Windows.LPETOptionsDaemonicFireCombo.value = LPET_CONFIG.DaemonicFire.mode
  LPET.OPTIONS.Windows.LPETOptionsWarpingEnergyCombo.value = LPET_CONFIG.WarpingEnergy.mode
  LPET.OPTIONS.Windows.LPETOptionsFlameOfTzeentchCombo.value = LPET_CONFIG.FlameOfTzeentch.mode
  LPET.OPTIONS.Windows.LPETOptionsFlamesOfChangeCombo.value = LPET_CONFIG.FlamesOfChange.mode
  LPET.OPTIONS.Windows.LPETOptionsCoruscatingEnergyCombo.value = LPET_CONFIG.CoruscatingEnergy.mode
  LPET.OPTIONS.Windows.LPETOptionsDaemonicConsumptionCombo.value = LPET_CONFIG.DaemonicConsumption.mode
  
  --Priority Settings
  LPET.OPTIONS.Windows.LPETOptionsDemonPriorityCombo.value = LPET.quickPriority
  LPET.OPTIONS.Windows.LPETOptionsDaemonicFirePriorityCombo.value = LPET_CONFIG.DaemonicFire.priority
  LPET.OPTIONS.Windows.LPETOptionsWarpingEnergyPriorityCombo.value = LPET_CONFIG.WarpingEnergy.priority
  LPET.OPTIONS.Windows.LPETOptionsFlameOfTzeentchPriorityCombo.value = LPET_CONFIG.FlameOfTzeentch.priority
  LPET.OPTIONS.Windows.LPETOptionsFlamesOfChangePriorityCombo.value = LPET_CONFIG.FlamesOfChange.priority
  LPET.OPTIONS.Windows.LPETOptionsCoruscatingEnergyPriorityCombo.value = LPET_CONFIG.CoruscatingEnergy.priority
  LPET.OPTIONS.Windows.LPETOptionsDaemonicConsumptionPriorityCombo.value = LPET_CONFIG.DaemonicConsumption.priority
  
  --Health Limit Settings
  LPET.OPTIONS.Windows.LPETOptionsDemonHealthCombo.value = (LPET.quickHealthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsDaemonicFireHealthCombo.value = (LPET_CONFIG.DaemonicFire.healthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsWarpingEnergyHealthCombo.value = (LPET_CONFIG.WarpingEnergy.healthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsFlameOfTzeentchHealthCombo.value = (LPET_CONFIG.FlameOfTzeentch.healthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsFlamesOfChangeHealthCombo.value = (LPET_CONFIG.FlamesOfChange.healthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsCoruscatingEnergyHealthCombo.value = (LPET_CONFIG.CoruscatingEnergy.healthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsDaemonicConsumptionHealthCombo.value = (LPET_CONFIG.DaemonicConsumption.healthLimit / 5) + 1
end

function LPET.DemonComboOnSelChanged(index)
  LPET.quickMode = index
  LPET.OPTIONS.Windows.LPETOptionsDemonCombo.value = index
  LPET.SetAllCastModes(index)
end

function LPET.DaemonicFireComboOnSelChanged(index)
  LPET_CONFIG.DaemonicFire.mode = index
  LPET.OPTIONS.Windows.LPETOptionsDaemonicFireCombo.value = index
  LPET.UpdatePetAbilitySettings()
end

function LPET.WarpingEnergyComboOnSelChanged(index)
  LPET_CONFIG.WarpingEnergy.mode = index
  LPET.OPTIONS.Windows.LPETOptionsWarpingEnergyCombo.value = index
  LPET.UpdatePetAbilitySettings()
end

function LPET.FlameOfTzeentchComboOnSelChanged(index)
  LPET_CONFIG.FlameOfTzeentch.mode = index
  LPET.OPTIONS.Windows.LPETOptionsFlameOfTzeentchCombo.value = index
  LPET.UpdatePetAbilitySettings()
end

function LPET.FlamesOfChangeComboOnSelChanged(index)
  LPET_CONFIG.FlamesOfChange.mode = index
  LPET.OPTIONS.Windows.LPETOptionsFlamesOfChangeCombo.value = index
  LPET.UpdatePetAbilitySettings()
end

function LPET.CoruscatingEnergyComboOnSelChanged(index)
  LPET_CONFIG.CoruscatingEnergy.mode = index
  LPET.OPTIONS.Windows.LPETOptionsCoruscatingEnergyCombo.value = index
  LPET.UpdatePetAbilitySettings()
end

function LPET.DaemonicConsumptionComboOnSelChanged(index)
  LPET_CONFIG.DaemonicConsumption.mode = index
  LPET.OPTIONS.Windows.LPETOptionsDaemonicConsumptionCombo.value = index
  LPET.UpdatePetAbilitySettings()
end
--[[
  Priority Settings
--]]
function LPET.DemonPriorityComboOnSelChanged(index)
  LPET.quickPriority = index
  LPET.OPTIONS.Windows.LPETOptionsDemonPriorityCombo.value = index
  LPET.SetAllPriorities(index)
end

function LPET.DaemonicFirePriorityComboOnSelChanged(index)
  LPET_CONFIG.DaemonicFire.priority = index
  LPET.OPTIONS.Windows.LPETOptionsDaemonicFirePriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end

function LPET.WarpingEnergyPriorityComboOnSelChanged(index)
  LPET_CONFIG.WarpingEnergy.priority = index
  LPET.OPTIONS.Windows.LPETOptionsWarpingEnergyPriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end

function LPET.FlameOfTzeentchPriorityComboOnSelChanged(index)
  LPET_CONFIG.FlameOfTzeentch.priority = index
  LPET.OPTIONS.Windows.LPETOptionsFlameOfTzeentchPriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end

function LPET.FlamesOfChangePriorityComboOnSelChanged(index)
  LPET_CONFIG.FlamesOfChange.priority = index
  LPET.OPTIONS.Windows.LPETOptionsFlamesOfChangePriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end

function LPET.CoruscatingEnergyPriorityComboOnSelChanged(index)
  LPET_CONFIG.CoruscatingEnergy.priority = index
  LPET.OPTIONS.Windows.LPETOptionsCoruscatingEnergyPriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end

function LPET.DaemonicConsumptionPriorityComboOnSelChanged(index)
  LPET_CONFIG.DaemonicConsumption.priority = index
  LPET.OPTIONS.Windows.LPETOptionsDaemonicConsumptionPriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end
--[[
  Health Limit Settings
--]]
function LPET.DemonHealthComboOnSelChanged(index)
  LPET.quickHealthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsDemonHealthCombo.value = index
  LPET.SetAllHealthLimits(LPET.quickHealthLimit)
end

function LPET.DaemonicFireHealthComboOnSelChanged(index)
  LPET_CONFIG.DaemonicFire.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsDaemonicFireHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.WarpingEnergyHealthComboOnSelChanged(index)
  LPET_CONFIG.WarpingEnergy.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsWarpingEnergyHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.FlameOfTzeentchHealthComboOnSelChanged(index)
  LPET_CONFIG.FlameOfTzeentch.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsFlameOfTzeentchHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.FlamesOfChangeHealthComboOnSelChanged(index)
  LPET_CONFIG.FlamesOfChange.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsFlamesOfChangeHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.CoruscatingEnergyHealthComboOnSelChanged(index)
  LPET_CONFIG.CoruscatingEnergy.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsCoruscatingEnergyHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.DaemonicConsumptionHealthComboOnSelChanged(index)
  LPET_CONFIG.DaemonicConsumption.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsDaemonicConsumptionHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end