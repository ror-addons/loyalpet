LPET.OPTIONS = {}

LPET.OPTIONS.Windows = {}
LPET.OPTIONS.Windows.LPETOptionsTitle= { type="title", career=LPET_CONSTANTS.ALL_CAREERS,  label = LPET.LOCALS.OPTION.TITLE.label, tooltiptext = LPET.LOCALS.OPTION.TITLE.tooltiptext}

--TextBox
LPET.OPTIONS.Windows.LPETOptionsProfilesEdit= {type="text", career=LPET_CONSTANTS.ALL_CAREERS, label=LPET.LOCALS.OPTION.PROFILESEDITTITLE.label, tooltiptext=LPET.LOCALS.OPTION.PROFILESEDITTITLE.tooltiptext, value = L""}

--Labels
LPET.OPTIONS.Windows.LPETOptionsAutoSwitchLabel= {type="label", career=LPET_CONSTANTS.ALL_CAREERS, label=LPET.LOCALS.OPTION.AUTOSWITCHTITLE.label, tooltiptext=LPET.LOCALS.OPTION.AUTOSWITCHTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsSwitchRangeCheckLabel= {type="label", career=LPET_CONSTANTS.ALL_CAREERS, label=LPET.LOCALS.OPTION.SWITCHRANGECHECKTITLE.label, tooltiptext=LPET.LOCALS.OPTION.SWITCHRANGECHECKTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsAutoAttackLabel= {type="label", career=LPET_CONSTANTS.ALL_CAREERS, label=LPET.LOCALS.OPTION.AUTOATTACKTITLE.label, tooltiptext=LPET.LOCALS.OPTION.AUTOATTACKTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsAttackRangeCheckLabel= {type="label", career=LPET_CONSTANTS.ALL_CAREERS, label=LPET.LOCALS.OPTION.ATTACKRANGECHECKTITLE.label, tooltiptext=LPET.LOCALS.OPTION.ATTACKRANGECHECKTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsAutoDefendLabel= {type="label", career=LPET_CONSTANTS.ALL_CAREERS, label=LPET.LOCALS.OPTION.AUTODEFENDTITLE.label, tooltiptext=LPET.LOCALS.OPTION.AUTODEFENDTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsDefendRangeCheckLabel= {type="label", career=LPET_CONSTANTS.ALL_CAREERS, label=LPET.LOCALS.OPTION.DEFENDRANGECHECKTITLE.label, tooltiptext=LPET.LOCALS.OPTION.DEFENDRANGECHECKTITLE.tooltiptext}
--LPET.OPTIONS.Windows.LPETOptionsAutoFollowLabel= {type="label", career=LPET_CONSTANTS.ALL_CAREERS, label=LPET.LOCALS.OPTION.AUTOFOLLOWTITLE.label, tooltiptext=LPET.LOCALS.OPTION.AUTOFOLLOWTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsSelfFollowLabel= {type="label", career=LPET_CONSTANTS.ALL_CAREERS, label=LPET.LOCALS.OPTION.SELFFOLLOWTITLE.label, tooltiptext=LPET.LOCALS.OPTION.SELFFOLLOWTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsPetAttackLabel= {type="label", career=LPET_CONSTANTS.ALL_CAREERS, label=LPET.LOCALS.OPTION.PETATTACKTITLE.label, tooltiptext=LPET.LOCALS.OPTION.PETATTACKTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsPetFollowLabel= {type="label", career=LPET_CONSTANTS.ALL_CAREERS, label=LPET.LOCALS.OPTION.PETFOLLOWTITLE.label, tooltiptext=LPET.LOCALS.OPTION.PETFOLLOWTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsAbilityNameLabel= {type="label", career=LPET_CONSTANTS.ALL_CAREERS, label=LPET.LOCALS.OPTION.ABILITYNAMETITLE.label, tooltiptext=LPET.LOCALS.OPTION.ABILITYNAMETITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsAbilityModeLabel= {type="label", career=LPET_CONSTANTS.ALL_CAREERS, label=LPET.LOCALS.OPTION.ABILITYMODETITLE.label, tooltiptext=LPET.LOCALS.OPTION.ABILITYMODETITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsAbilityPriorityLabel= {type="label", career=LPET_CONSTANTS.ALL_CAREERS, label=LPET.LOCALS.OPTION.ABILITYPRIORITYTITLE.label, tooltiptext=LPET.LOCALS.OPTION.ABILITYPRIORITYTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsAbilityHealthLabel= {type="label", career=LPET_CONSTANTS.ALL_CAREERS, label=LPET.LOCALS.OPTION.ABILITYHEALTHTITLE.label, tooltiptext=LPET.LOCALS.OPTION.ABILITYHEALTHTITLE.tooltiptext}

--Tabs & Buttons
LPET.OPTIONS.Windows.LPETOptionsTabAuto = { type="button", career=LPET_CONSTANTS.ALL_CAREERS, label = LPET.LOCALS.OPTION.AUTOTAB.label, tooltiptext = LPET.LOCALS.OPTION.AUTOTAB.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsTabManual = { type="button", career=LPET_CONSTANTS.ALL_CAREERS, label = LPET.LOCALS.OPTION.MANUALTAB.label, tooltiptext = LPET.LOCALS.OPTION.MANUALTAB.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsTabAbilities = { type="button", career=LPET_CONSTANTS.ALL_CAREERS, label = LPET.LOCALS.OPTION.ABILITIESTAB.label, tooltiptext = LPET.LOCALS.OPTION.ABILITIESTAB.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsTabProfiles = { type="button", career=LPET_CONSTANTS.ALL_CAREERS, label = LPET.LOCALS.OPTION.PROFILESTAB.label, tooltiptext = LPET.LOCALS.OPTION.PROFILESTAB.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsSaveProfileButton = { type="button", career=LPET_CONSTANTS.ALL_CAREERS, label = LPET.LOCALS.OPTION.SAVEBUTTON.label, tooltiptext = LPET.LOCALS.OPTION.SAVEBUTTON.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsLoadProfileButton = { type="button", career=LPET_CONSTANTS.ALL_CAREERS, label = LPET.LOCALS.OPTION.LOADBUTTON.label, tooltiptext = LPET.LOCALS.OPTION.LOADBUTTON.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsRenameProfileButton = { type="button", career=LPET_CONSTANTS.ALL_CAREERS, label = LPET.LOCALS.OPTION.RENAMEBUTTON.label, tooltiptext = LPET.LOCALS.OPTION.RENAMEBUTTON.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsAddProfileButton = { type="button", career=LPET_CONSTANTS.ALL_CAREERS, label = LPET.LOCALS.OPTION.ADDBUTTON.label, tooltiptext = LPET.LOCALS.OPTION.ADDBUTTON.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsRemoveProfileButton = { type="button", career=LPET_CONSTANTS.ALL_CAREERS, label = LPET.LOCALS.OPTION.REMOVEBUTTON.label, tooltiptext = LPET.LOCALS.OPTION.REMOVEBUTTON.tooltiptext}

--ComboBoxes
LPET.OPTIONS.Windows.LPETOptionsQuickSettingsCombo = {type="combo", career=LPET_CONSTANTS.ALL_CAREERS, label = L"", tooltiptext = L"Use this to set all settings to same value at once", value = LPET.quickSettings, data = LPET.LOCALS.OPTION.ENABLE.data}
LPET.OPTIONS.Windows.LPETOptionsAutoSwitchCombo = {type="combo", career=LPET_CONSTANTS.ALL_CAREERS, label = LPET.LOCALS.OPTION.ENABLE.label, tooltiptext = LPET.LOCALS.OPTION.ENABLE.tooltiptext, value = LPET_CONFIG.autoswitch, data = LPET.LOCALS.OPTION.ENABLE.data}
LPET.OPTIONS.Windows.LPETOptionsSwitchRangeCheckCombo = {type="combo", career=LPET_CONSTANTS.ALL_CAREERS, label = LPET.LOCALS.OPTION.ENABLE.label, tooltiptext = LPET.LOCALS.OPTION.ENABLE.tooltiptext, value = LPET_CONFIG.switchrangecheck, data = LPET.LOCALS.OPTION.ENABLE.data}
LPET.OPTIONS.Windows.LPETOptionsAutoAttackCombo = {type="combo", career=LPET_CONSTANTS.ALL_CAREERS, label = LPET.LOCALS.OPTION.ENABLE.label, tooltiptext = LPET.LOCALS.OPTION.ENABLE.tooltiptext, value = LPET_CONFIG.autoattack, data = LPET.LOCALS.OPTION.ENABLE.data}
LPET.OPTIONS.Windows.LPETOptionsAttackRangeCheckCombo = {type="combo", career=LPET_CONSTANTS.ALL_CAREERS, label = LPET.LOCALS.OPTION.ENABLE.label, tooltiptext = LPET.LOCALS.OPTION.ENABLE.tooltiptext, value = LPET_CONFIG.attackrangecheck, data = LPET.LOCALS.OPTION.ENABLE.data}
LPET.OPTIONS.Windows.LPETOptionsAutoDefendCombo = {type="combo", career=LPET_CONSTANTS.ALL_CAREERS, label = LPET.LOCALS.OPTION.ENABLE.label, tooltiptext = LPET.LOCALS.OPTION.ENABLE.tooltiptext, value = LPET_CONFIG.autodefend, data = LPET.LOCALS.OPTION.ENABLE.data}
LPET.OPTIONS.Windows.LPETOptionsDefendRangeCheckCombo = {type="combo", career=LPET_CONSTANTS.ALL_CAREERS, label = LPET.LOCALS.OPTION.ENABLE.label, tooltiptext = LPET.LOCALS.OPTION.ENABLE.tooltiptext, value = LPET_CONFIG.defendrangecheck, data = LPET.LOCALS.OPTION.ENABLE.data}
--LPET.OPTIONS.Windows.LPETOptionsAutoFollowCombo = {type="combo", career=LPET_CONSTANTS.ALL_CAREERS, label = LPET.LOCALS.OPTION.ENABLE.label, tooltiptext = LPET.LOCALS.OPTION.ENABLE.tooltiptext, value = LPET_CONFIG.autofollow, data = LPET.LOCALS.OPTION.ENABLE.data}
LPET.OPTIONS.Windows.LPETOptionsSelfFollowCombo = {type="combo", career=LPET_CONSTANTS.ALL_CAREERS, label = LPET.LOCALS.OPTION.ENABLE.label, tooltiptext = LPET.LOCALS.OPTION.ENABLE.tooltiptext, value = LPET_CONFIG.selftargetfollow, data = LPET.LOCALS.OPTION.ENABLE.data}
LPET.OPTIONS.Windows.LPETOptionsPetAttackCombo = {type="combo", career=LPET_CONSTANTS.ALL_CAREERS, label = LPET.LOCALS.OPTION.BUTTONS.label, tooltiptext = LPET.LOCALS.OPTION.BUTTONS.tooltiptext, value = LPET_CONFIG.attackbuttoncombo, data = LPET.LOCALS.OPTION.BUTTONS.data}
LPET.OPTIONS.Windows.LPETOptionsPetFollowCombo = {type="combo", career=LPET_CONSTANTS.ALL_CAREERS, label = LPET.LOCALS.OPTION.BUTTONS.label, tooltiptext = LPET.LOCALS.OPTION.BUTTONS.tooltiptext, value = LPET_CONFIG.followbuttoncombo, data = LPET.LOCALS.OPTION.BUTTONS.data}
LPET.OPTIONS.Windows.LPETOptionsProfilesCombo = {type="combo", career=LPET_CONSTANTS.ALL_CAREERS, label = LPET.LOCALS.OPTION.PROFILES.label, tooltiptext = LPET.LOCALS.OPTION.PROFILES.tooltiptext, value = LPET.currentProfileSelected, data = LPET.LOCALS.OPTION.PROFILES.data}

--Tab windows
LPET.OPTIONS.TABS = {}
LPET.OPTIONS.TABS = { 
  LPETOptionsTabAuto = "LPETOptionsAutoWindow",
  LPETOptionsTabManual = "LPETOptionsManualWindow",
  LPETOptionsTabAbilities = "LPETOptionsAbilitiesWindow",
  LPETOptionsTabProfiles = "LPETOptionsProfilesWindow",
}