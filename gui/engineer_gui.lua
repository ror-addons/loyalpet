-- Labels
LPET.OPTIONS.Windows.LPETOptionsPenetratingRoundLabel= {type="label", career=LPET_CONSTANTS.ENGINEER, label=LPET.LOCALS.OPTION.PENETRATINGROUNDTITLE.label, tooltiptext=LPET.LOCALS.OPTION.PENETRATINGROUNDTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsFlamethrowerLabel= {type="label", career=LPET_CONSTANTS.ENGINEER, label=LPET.LOCALS.OPTION.FLAMETHROWERTITLE.label, tooltiptext=LPET.LOCALS.OPTION.FLAMETHROWERTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsShockGrenadeLabel= {type="label", career=LPET_CONSTANTS.ENGINEER, label=LPET.LOCALS.OPTION.SHOCKGRENADETITLE.label, tooltiptext=LPET.LOCALS.OPTION.SHOCKGRENADETITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsHighExplosiveGrenadeLabel= {type="label", career=LPET_CONSTANTS.ENGINEER, label=LPET.LOCALS.OPTION.HIGHEXPLOSIVEGRENADETITLE.label, tooltiptext=LPET.LOCALS.OPTION.HIGHEXPLOSIVEGRENADETITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsMachineGunLabel= {type="label", career=LPET_CONSTANTS.ENGINEER, label=LPET.LOCALS.OPTION.MACHINEGUNTITLE.label, tooltiptext=LPET.LOCALS.OPTION.MACHINEGUNTITLE.tooltiptext}
LPET.OPTIONS.Windows.LPETOptionsSteamVentLabel= {type="label", career=LPET_CONSTANTS.ENGINEER, label=LPET.LOCALS.OPTION.STEAMVENTTITLE.label, tooltiptext=LPET.LOCALS.OPTION.STEAMVENTTITLE.tooltiptext}

-- Combos
LPET.OPTIONS.Windows.LPETOptionsTurretCombo = {type="combo", career=LPET_CONSTANTS.ENGINEER, label = L"", tooltiptext = L"Use this to set all modes to same value at once", value = LPET.quickMode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsPenetratingRoundCombo = {type="combo", career=LPET_CONSTANTS.ENGINEER, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.PenetratingRound.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsFlamethrowerCombo = {type="combo", career=LPET_CONSTANTS.ENGINEER, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.Flamethrower.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsShockGrenadeCombo = {type="combo", career=LPET_CONSTANTS.ENGINEER, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.ShockGrenade.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsHighExplosiveGrenadeCombo = {type="combo", career=LPET_CONSTANTS.ENGINEER, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.HighExplosiveGrenade.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsMachineGunCombo = {type="combo", career=LPET_CONSTANTS.ENGINEER, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.MachineGun.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}
LPET.OPTIONS.Windows.LPETOptionsSteamVentCombo = {type="combo", career=LPET_CONSTANTS.ENGINEER, label = LPET.LOCALS.OPTION.ABILITYUSE.label, tooltiptext = LPET.LOCALS.OPTION.ABILITYUSE.tooltiptext, value = LPET_CONFIG.SteamVent.mode, data = LPET.LOCALS.OPTION.ABILITYUSE.data}

LPET.OPTIONS.Windows.LPETOptionsTurretPriorityCombo = {type="combo", career=LPET_CONSTANTS.ENGINEER, label = L"", tooltiptext = L"Use this to set all priorities to same value at once", value = LPET.quickPriority, data = LPET.LOCALS.OPTION.PRIORITY2.data}
LPET.OPTIONS.Windows.LPETOptionsPenetratingRoundPriorityCombo = {type="combo", career=LPET_CONSTANTS.ENGINEER, label = LPET.LOCALS.OPTION.PRIORITY2.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY2.tooltiptext, value = LPET_CONFIG.PenetratingRound.priority, data = LPET.LOCALS.OPTION.PRIORITY2.data}
LPET.OPTIONS.Windows.LPETOptionsFlamethrowerPriorityCombo = {type="combo", career=LPET_CONSTANTS.ENGINEER, label = LPET.LOCALS.OPTION.PRIORITY2.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY2.tooltiptext, value = LPET_CONFIG.Flamethrower.priority, data = LPET.LOCALS.OPTION.PRIORITY2.data}
LPET.OPTIONS.Windows.LPETOptionsShockGrenadePriorityCombo = {type="combo", career=LPET_CONSTANTS.ENGINEER, label = LPET.LOCALS.OPTION.PRIORITY2.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY2.tooltiptext, value = LPET_CONFIG.ShockGrenade.priority, data = LPET.LOCALS.OPTION.PRIORITY2.data}
LPET.OPTIONS.Windows.LPETOptionsHighExplosiveGrenadePriorityCombo = {type="combo", career=LPET_CONSTANTS.ENGINEER, label = LPET.LOCALS.OPTION.PRIORITY2.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY2.tooltiptext, value = LPET_CONFIG.HighExplosiveGrenade.priority, data = LPET.LOCALS.OPTION.PRIORITY2.data}
LPET.OPTIONS.Windows.LPETOptionsMachineGunPriorityCombo = {type="combo", career=LPET_CONSTANTS.ENGINEER, label = LPET.LOCALS.OPTION.PRIORITY2.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY2.tooltiptext, value = LPET_CONFIG.MachineGun.priority, data = LPET.LOCALS.OPTION.PRIORITY2.data}
LPET.OPTIONS.Windows.LPETOptionsSteamVentPriorityCombo = {type="combo", career=LPET_CONSTANTS.ENGINEER, label = LPET.LOCALS.OPTION.PRIORITY2.label, tooltiptext = LPET.LOCALS.OPTION.PRIORITY2.tooltiptext, value = LPET_CONFIG.SteamVent.priority, data = LPET.LOCALS.OPTION.PRIORITY2.data}

LPET.OPTIONS.Windows.LPETOptionsTurretHealthCombo = {type="combo", career=LPET_CONSTANTS.ENGINEER, label = L"", tooltiptext = L"Use this to set all health limits to same value at once", value = (LPET.quickHealthLimit / 5) + 1, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsPenetratingRoundHealthCombo = {type="combo", career=LPET_CONSTANTS.ENGINEER, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = (LPET_CONFIG.PenetratingRound.healthLimit / 5) + 1, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsFlamethrowerHealthCombo = {type="combo", career=LPET_CONSTANTS.ENGINEER, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = (LPET_CONFIG.Flamethrower.healthLimit / 5) + 1, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsShockGrenadeHealthCombo = {type="combo", career=LPET_CONSTANTS.ENGINEER, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = (LPET_CONFIG.ShockGrenade.healthLimit / 5) + 1, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsHighExplosiveGrenadeHealthCombo = {type="combo", career=LPET_CONSTANTS.ENGINEER, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = (LPET_CONFIG.HighExplosiveGrenade.healthLimit / 5) + 1, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsMachineGunHealthCombo = {type="combo", career=LPET_CONSTANTS.ENGINEER, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = (LPET_CONFIG.MachineGun.healthLimit / 5) + 1, data = LPET.LOCALS.OPTION.HEALTH.data}
LPET.OPTIONS.Windows.LPETOptionsSteamVentHealthCombo = {type="combo", career=LPET_CONSTANTS.ENGINEER, label = LPET.LOCALS.OPTION.HEALTH.label, tooltiptext = LPET.LOCALS.OPTION.HEALTH.tooltiptext, value = (LPET_CONFIG.SteamVent.healthLimit / 5) + 1, data = LPET.LOCALS.OPTION.HEALTH.data}

-- Functions
function LPET.UpdateEngineerGUI()
  --Ability Settings
  LPET.OPTIONS.Windows.LPETOptionsTurretCombo.value = LPET.quickMode
  LPET.OPTIONS.Windows.LPETOptionsPenetratingRoundCombo.value = LPET_CONFIG.PenetratingRound.mode
  LPET.OPTIONS.Windows.LPETOptionsFlamethrowerCombo.value = LPET_CONFIG.Flamethrower.mode
  LPET.OPTIONS.Windows.LPETOptionsShockGrenadeCombo.value = LPET_CONFIG.ShockGrenade.mode
  LPET.OPTIONS.Windows.LPETOptionsHighExplosiveGrenadeCombo.value = LPET_CONFIG.HighExplosiveGrenade.mode
  LPET.OPTIONS.Windows.LPETOptionsMachineGunCombo.value = LPET_CONFIG.MachineGun.mode
  LPET.OPTIONS.Windows.LPETOptionsSteamVentCombo.value = LPET_CONFIG.SteamVent.mode
  
  --Priority Settings
  LPET.OPTIONS.Windows.LPETOptionsTurretPriorityCombo.value = LPET.quickPriority
  LPET.OPTIONS.Windows.LPETOptionsPenetratingRoundPriorityCombo.value = LPET_CONFIG.PenetratingRound.priority
  LPET.OPTIONS.Windows.LPETOptionsFlamethrowerPriorityCombo.value = LPET_CONFIG.Flamethrower.priority
  LPET.OPTIONS.Windows.LPETOptionsShockGrenadePriorityCombo.value = LPET_CONFIG.ShockGrenade.priority
  LPET.OPTIONS.Windows.LPETOptionsHighExplosiveGrenadePriorityCombo.value = LPET_CONFIG.HighExplosiveGrenade.priority
  LPET.OPTIONS.Windows.LPETOptionsMachineGunPriorityCombo.value = LPET_CONFIG.MachineGun.priority
  LPET.OPTIONS.Windows.LPETOptionsSteamVentPriorityCombo.value = LPET_CONFIG.SteamVent.priority
  
  --Health Limit Settings
  LPET.OPTIONS.Windows.LPETOptionsTurretHealthCombo.value = (LPET.quickHealthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsPenetratingRoundHealthCombo.value = (LPET_CONFIG.PenetratingRound.healthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsFlamethrowerHealthCombo.value = (LPET_CONFIG.Flamethrower.healthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsShockGrenadeHealthCombo.value = (LPET_CONFIG.ShockGrenade.healthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsHighExplosiveGrenadeHealthCombo.value = (LPET_CONFIG.HighExplosiveGrenade.healthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsMachineGunHealthCombo.value = (LPET_CONFIG.MachineGun.healthLimit / 5) + 1
  LPET.OPTIONS.Windows.LPETOptionsSteamVentHealthCombo.value = (LPET_CONFIG.SteamVent.healthLimit / 5) + 1
end
--[[
  Ability Settings
--]]
function LPET.TurretComboOnSelChanged(index)
  LPET.quickMode = index
  LPET.OPTIONS.Windows.LPETOptionsTurretCombo.value = index
  LPET.SetAllCastModes(index)
end

function LPET.PenetratingRoundComboOnSelChanged(index)
  LPET_CONFIG.PenetratingRound.mode = index
  LPET.OPTIONS.Windows.LPETOptionsPenetratingRoundCombo.value = index
  LPET.UpdatePetAbilitySettings()
end

function LPET.FlamethrowerComboOnSelChanged(index)
  LPET_CONFIG.Flamethrower.mode = index
  LPET.OPTIONS.Windows.LPETOptionsFlamethrowerCombo.value = index
  LPET.UpdatePetAbilitySettings()
end

function LPET.ShockGrenadeComboOnSelChanged(index)
  LPET_CONFIG.ShockGrenade.mode = index
  LPET.OPTIONS.Windows.LPETOptionsShockGrenadeCombo.value = index
  LPET.UpdatePetAbilitySettings()
end

function LPET.HighExplosiveGrenadeComboOnSelChanged(index)
  LPET_CONFIG.HighExplosiveGrenade.mode = index
  LPET.OPTIONS.Windows.LPETOptionsHighExplosiveGrenadeCombo.value = index
  LPET.UpdatePetAbilitySettings()
end

function LPET.MachineGunComboOnSelChanged(index)
  LPET_CONFIG.MachineGun.mode = index
  LPET.OPTIONS.Windows.LPETOptionsMachineGunCombo.value = index
  LPET.UpdatePetAbilitySettings()
end

function LPET.SteamVentComboOnSelChanged(index)
  LPET_CONFIG.SteamVent.mode = index
  LPET.OPTIONS.Windows.LPETOptionsSteamVentCombo.value = index
  LPET.UpdatePetAbilitySettings()
end
--[[
  Priority Settings
--]]
function LPET.TurretPriorityComboOnSelChanged(index)
  LPET.quickPriority = index
  LPET.OPTIONS.Windows.LPETOptionsTurretPriorityCombo.value = index
  LPET.SetAllPriorities(index)
end

function LPET.PenetratingRoundPriorityComboOnSelChanged(index)
  LPET_CONFIG.PenetratingRound.priority = index
  LPET.OPTIONS.Windows.LPETOptionsPenetratingRoundPriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end

function LPET.FlamethrowerPriorityComboOnSelChanged(index)
  LPET_CONFIG.Flamethrower.priority = index
  LPET.OPTIONS.Windows.LPETOptionsFlamethrowerPriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end

function LPET.ShockGrenadePriorityComboOnSelChanged(index)
  LPET_CONFIG.ShockGrenade.priority = index
  LPET.OPTIONS.Windows.LPETOptionsShockGrenadePriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end

function LPET.HighExplosiveGrenadePriorityComboOnSelChanged(index)
  LPET_CONFIG.HighExplosiveGrenade.priority = index
  LPET.OPTIONS.Windows.LPETOptionsHighExplosiveGrenadePriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end

function LPET.MachineGunPriorityComboOnSelChanged(index)
  LPET_CONFIG.MachineGun.priority = index
  LPET.OPTIONS.Windows.LPETOptionsMachineGunPriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end

function LPET.SteamVentPriorityComboOnSelChanged(index)
  LPET_CONFIG.SteamVent.priority = index
  LPET.OPTIONS.Windows.LPETOptionsSteamVentPriorityCombo.value = index
  LPET.UpdatePetPrioritySettings()
end
--[[
  Pet Target Health Limit Settings
--]]
function LPET.TurretHealthComboOnSelChanged(index)
  LPET.quickHealthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsTurretHealthCombo.value = index
  LPET.SetAllHealthLimits(LPET.quickHealthLimit)
end

function LPET.PenetratingRoundHealthComboOnSelChanged(index)
  LPET_CONFIG.PenetratingRound.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsPenetratingRoundHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.FlamethrowerHealthComboOnSelChanged(index)
  LPET_CONFIG.Flamethrower.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsFlamethrowerHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.ShockGrenadeHealthComboOnSelChanged(index)
  LPET_CONFIG.ShockGrenade.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsShockGrenadeHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.HighExplosiveGrenadeHealthComboOnSelChanged(index)
  LPET_CONFIG.HighExplosiveGrenade.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsHighExplosiveGrenadeHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.MachineGunHealthComboOnSelChanged(index)
  LPET_CONFIG.MachineGun.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsMachineGunHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end

function LPET.SteamVentHealthComboOnSelChanged(index)
  LPET_CONFIG.SteamVent.healthLimit = (index - 1) * 5
  LPET.OPTIONS.Windows.LPETOptionsSteamVentHealthCombo.value = index
  LPET.UpdatePetHealthLimitSettings()
end